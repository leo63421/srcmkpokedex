import './App.css';
import { useEffect, useState } from 'react';
import axios from 'axios';

function App() {

  const [pokemon, setPokemon] = useState();
  const [id, setId] = useState(1);


  async function getPokemonById(id) {
    try {
      const res = await axios.get(`https://pokeapi.co/api/v2/pokemon/${id}`);
      setPokemon(res.data);
    } catch (e) {
      console.log(e);
    }
  }

  useEffect(() => {
    getPokemonById(id);
  }, [])

  useEffect(() => {
    getPokemonById(id);
  }, [id])

  return (
    <div className="App">
      <h1>
        Pokedex!
      </h1>
      <h5>
        Having fun with pokemon API
      </h5>
      <h5>
        {'API: https://pokeapi.co/api/v2/pokemon/<id>'}
      </h5>
      <h5>
        {'Reference: '} 
        <a href="https://pokeapi.co/">https://pokeapi.co/</a>
      </h5>
      <hr />
      <div>
        <button onClick={() => {setId(id-10)}} disabled={id <= 10}> {' << 10 '} </button>
        <button onClick={() => {setId(id-1)}} disabled={id === 1}> {' << '} </button>
        <button onClick={() => {setId(id+1)}}> {' >> '} </button>
        <button onClick={() => {setId(id+10)}}> {' 10 >> '} </button>
      </div>
      {pokemon ? <img src={pokemon.sprites.front_default} /> : <></>}
      {pokemon ? <div> {'#' + pokemon.id} </div> : <></>}
      {pokemon ? <div> {'Name: ' + pokemon.name} </div> : <></>}
      <div>
        Types:
      </div>
      <div>
        {pokemon ? pokemon.types.map((type, key) => {return <div key={key}> {type.type.name} </div>}) : <></>}
      </div>
    </div>
  );
}

export default App;
